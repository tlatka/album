import 'dart:io';

import 'package:flutter/material.dart';
import 'package:storage_path/storage_path.dart';
import 'dart:convert';

void main() => runApp(new MyApp());

class MainPage extends StatefulWidget {
  final String title;

  @override MainPage({this.title}) : super();

  @override State<StatefulWidget> createState() {
    return new MainPageState();
  }

}
class MainPageState extends State<MainPage> {

  List<String> _imagePathList;
  List<Widget> _gridTiles = [];
  int _imagesNum = 0;

  @override
  void initState() {
    super.initState();
    getImageList();
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new GridView.builder(
        itemCount: _imagesNum,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return _getImage(index);
        }
      ),
    );
  }

  Future getImageList() async {
    var imagesJsonStr = await StoragePath.imagesPath;
    var imagesJsonDecoded = jsonDecode(imagesJsonStr);
    var firstScore = imagesJsonDecoded[0];

    setState(() {
      _imagePathList = firstScore['files'].cast<String>();
      _imagesNum = _imagePathList.length;
  });
  }
  _getImage(int index) {
      var imageFile = new File(_imagePathList[index]);
      Image imageImage = new Image.file(imageFile, fit: BoxFit.fill, scale: 0.1);
      return new Container(child: imageImage);
  }
  
  /*List<Widget> _buildGridTiles() {

    List<Container> containers = [];

    if (_imagePathList == null) {
      return containers;
    }

    for (int i = 0; i < _imagePathList.length; i++) {
    //for (int i = 0; i < 20; i++) {
      var imageFile = new File(_imagePathList[i]);
      Image imageImage = new Image.file(imageFile, fit: BoxFit.fill, scale: 0.1);
      Container imageContainer = new Container(child: imageImage);
      containers.add(imageContainer);
    }
    print("ala ma kota 2");
    return containers;
  }
*/
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "",
      home: new MainPage(title: "GridView of Images")
    );
  }
}