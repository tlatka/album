import 'dart:io';

import 'package:flutter/material.dart';
import 'package:storage_path/storage_path.dart';
import 'dart:convert';

void main() => runApp(new MyApp());

class MainPage extends StatefulWidget {
  final String title;

  @override MainPage({this.title}) : super();

  @override State<StatefulWidget> createState() {
    return new MainPageState();
  }

}
class MainPageState extends State<MainPage> {

  List<String> _imagePathList;
  int _imagesNum = 0;

  @override
  void initState() {
    super.initState();
    getImageList();
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new GridView.builder(
        itemCount: _imagesNum,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3, crossAxisSpacing: 10, mainAxisSpacing: 10),
        itemBuilder: (BuildContext context, int index) {
          return _getImage(index);
        }
      ),
    );
  }

  Future getImageList() async {
    var imagesJsonStr = await StoragePath.imagesPath;
    var imagesJsonDecoded = jsonDecode(imagesJsonStr);
    var firstScore = imagesJsonDecoded[0];

    setState(() {
      _imagePathList = firstScore['files'].cast<String>();
      _imagesNum = _imagePathList.length;
  });
  }

  _getImage(int index) {
      return new PhotoHero(
          photo: _imagePathList[index],
          //width: 300.0,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute<void>(
              builder: (BuildContext context) {
                return Scaffold(
                  appBar: AppBar(
                    title: const Text('Flippers Page'),
                  ),
                  body: Container(
                    // The blue background emphasizes that it's a new route.
                    color: Colors.lightBlueAccent,
                    padding: const EdgeInsets.all(16.0),
                    alignment: Alignment.topLeft,
                    child: PhotoHero(
                      photo: _imagePathList[index],
                      //width: 100.0,
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                );
              }
            ));
          },
      );
  }
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "",
      home: new MainPage(title: "Album")
    );
  }
}


class PhotoHero extends StatelessWidget {
  const PhotoHero({ Key key, this.photo, this.onTap }) : super(key: key);

  final String photo;
  final VoidCallback onTap;
  //final double width;

  Widget build(BuildContext context) {
    return SizedBox(
      //width: width,
      child: Hero(
        tag: photo,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: Image.file(File(photo), fit: BoxFit.fitWidth, cacheWidth: 200,),
            ),
          ),
        ),
      );
  }
}